{-# LANGUAGE OverloadedStrings, FlexibleInstances, UndecidableInstances #-}
module Main where

import CLIO
import Control.Exception
import qualified Data.ByteString.Char8 as B

data TaxpayerInfo =
  TI { taxpayerName :: B.ByteString
     , taxpayerSSN :: Integer
     , taxpayerIncome :: Integer
     , taxpayerWithheld :: Integer
     , taxpayerBankName :: B.ByteString
     , taxpayerBankAccount :: Integer }
  deriving (Eq, Show, Read)

emptyTPI = TI "No name" (-1) (-1) (-1) "No bank" (-1)
emptyTR = TR emptyTPI (0,0)

data TaxReturn = TR TaxpayerInfo (Float, Float)
  deriving (Eq, Show, Read)

instance (Show a, Read a) => Serializable a where
  serialize = B.pack . show
  deserialize b = case reads (B.unpack b) of
    [(a,_)] -> Right a
    _ -> Left "generic Read instance deserialize error"

ioPutStrLn s = ioPutStr (s ++ "\n")
ioPrint x = ioPutStrLn (show x)

tainted :: DCLabel -> DCLabel
tainted l = l { dcAvailability = cTrue }

irs = principal "IRS"
customer = principal "Customer"
preparer = principal "Preparer"
customerIRS = customer \/ irs %% customer
preparerIRS = preparer \/ irs %% preparer \/ customer
preparerCustomerIRS = preparer \/ customer \/ irs %% customer

data Bracket = Closed Integer Integer Float | Open Integer Float

taxRates = [ Closed 0 10 0.05
           , Closed 11 20 0.12
           , Closed 21 50 0.3
           , Open 51 0.7]

findBrackets n = filter (fitsBracket n)
  where fitsBracket n (Closed l u _) = l <= n
        fitsBracket n (Open l _) = l <= n

computeTax :: Integer -> Float
computeTax n =
  let bs = findBrackets n taxRates
  in go bs 0
  where go [] tax = tax
        go (Closed l u r:bs) tax =
          let t = min u n
          in go bs (tax + fromIntegral (t - l) * r)
        go (Open l r:bs) tax =
          go bs (tax + fromIntegral (n - l) * r)
           
prepareTaxes :: TaxpayerInfo -> TaxReturn
prepareTaxes tpi =
  let totalTax = computeTax (taxpayerIncome tpi)
      balance = totalTax - fromIntegral (taxpayerWithheld tpi)
  in TR tpi (totalTax, balance)
  
customerCode :: TaxpayerInfo -> DC ()
customerCode tpi =
  do info <- label preparerCustomerIRS tpi
     store "taxpayer_info" info
     return ()
     
preparerCode :: DC ()
preparerCode =
  do defaultInfo <- label (tainted preparerIRS) emptyTPI
     info <- fetch "taxpayer_info" defaultInfo
     r <- toLabeled (tainted preparerIRS) $ do
       i <- unlabel info
       return (prepareTaxes i)
     store "tax_return" r

irsCode :: DC Bool
irsCode =
  do defaultReturn <- label (tainted (irs %% True)) emptyTR
     lv <- fetch "tax_return" defaultReturn
     TR tpi (tax,balance) <- unlabel lv
     return $ verifyReturn tpi tax balance

verifyReturn tpi tax balance =
  computeTax (taxpayerIncome tpi) == tax
  && balance == tax - fromIntegral (taxpayerWithheld tpi)

mkTPI :: TaxpayerInfo
mkTPI =
  let income = 26
      withheld = 9
      name = "John Doe"
      ssn = 12345
      bankName = "Bank of America"
      account = 42
  in TI name ssn income withheld bankName account

taxPrep :: IO ()
taxPrep = do
  [irsKM, customerKM, preparerKM] <-
    mapM (initializeKeyMapIO . wrap) [irs,
                                      principal "Customer",
                                      principal "Preparer"]
  let public p km = adjustKey p SigningKey Nothing $
                     adjustKey p DecryptionKey Nothing km
  let km1 = customerKM <+> public preparer preparerKM <+> public irs irsKM
  let tpi = mkTPI
  putStrLn "Running the Customer"
  defaultClio (True %% customer) km1 (customerCode tpi)
  putStrLn "Press Enter to continue"
  getChar
  putStrLn "Running the Preparer"
  defaultClio (True %% preparer) (preparerKM <+> (public irs irsKM <+> public customer customerKM)) preparerCode
  putStrLn "Press Enter to continue"
  getChar
  putStrLn "The IRS is coming!"
  b <- defaultClio (True %% irs) (irsKM <+> public preparer preparerKM) irsCode
  if b
    then putStrLn "IRS says: OK"
    else putStrLn "IRS says: WRONG, your life is over"
  putStrLn "Press Enter to end"
  getChar
  return ()

main = taxPrep `finally` ioDBCleanup

defaultClio l km m =
  evalCLIO (dcDefaultState { clioPC = l, clioClr = DCLabel cFalse cTrue cTrue
                           , clioKeyMap = km
                           , clioStoreLabel = dcPublic })
    m
   `onException` ioDBCleanup

wrap :: a -> [a]
wrap x = [x]
