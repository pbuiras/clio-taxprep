# CLIO Tax Preparation example #

This package depends on [CLIO](https://bitbucket.org/pbuiras/clio).

## Installation in a cabal sandbox (preferred) ##

1. Clone the clio repo (no need to build it but make sure a local Redis db is up and running)
2. Clone the clio-taxprep repo
3. From the clio-taxprep repo, run:


```
#!bash
cabal sandbox init
cabal install <path-to-clio-source>/clio.cabal
cabal build
```

That should install all (Haskell) dependencies automatically.

## Installation in local package cache (will pollute your cabal!) ##

Clone the clio repo, then from that directory do
```
#!bash
cabal install
```
Clone the clio-taxprep repo, then from that directory do

```
#!bash
cabal install --dependencies-only
cabal build
```